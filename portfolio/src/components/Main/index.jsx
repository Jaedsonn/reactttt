import './style.css'
import Header from "./Header"
import Section from "./Section"

export default function Main(){
    return(
        <main>
            < Header />
            <Section></Section>
        </main>
    )
}