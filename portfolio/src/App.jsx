import Nav from './components/nav/index'
import Main from './components/Main'
import './App.css'

function App() {
  

  return (
    <>
      <Nav />
      <Main />
    </>
  )
}

export default App
